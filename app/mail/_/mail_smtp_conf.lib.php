<?php

### GYURAL ###

/*

------------
Gyural 1.8.3
------------

Filename: /app/mail/_/mail_smtp_conf.lib.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 29/03/2014

*/

class mail_smtp_conf extends standardObject {
	
	var $SMTPDebug = 0;
	var $SMTPAuth = 'login';
	var $SMTPSecure = 'ssl';
	var $Host = 'server';
	var $Port = 465;
	var $Username = 'username';
	var $Password = 'password';

}

?>