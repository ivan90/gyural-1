<?php

/*

----------
Gyural 1.8
----------

Filename: /app/mail/mail.funcs.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
    Date: 21/11/13

----------------
Collection: Mail
----------------

*/

function mail__save($to, $subject, $body) {

	$mail = LoadClass('mail', 1);
	$mail->setAttr('subject', $subject);
	$mail->setAttr('body', $body);
	$mail->setAttr('to', $to);
	$mail->setAttr('date', time());
	return $mail->hangExecute();

}

function mail__queue() {

	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: ' . mail . "\r\n";

	$email = LoadClass('mail', 1)->filter(array('send', 0), 'ORDER BY date ASC', array('LIMIT', 5));
	if(@is_array($email)) {
		foreach($email as $mail) {
			
			$mailHeader = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/><title>'.siteName.'</title></head><body>';
			$mail->body .= footer_email;

			$go = false;
			
			if(sendmail == 'smtp') {

				$configuration = LoadClass('mail/smtp/conf', 1);
				$smtp = LoadClass('mail/smtp', 1);

				$smtp->IsSMTP();
				$smtp->isHTML();
				$smtp->SMTPDebug = $configuration->SMTPDebug;
				$smtp->SMTPAuth = $configuration->SMTPAuth;
				$smtp->SMTPSecure = $configuration->SMTPSecure;
				$smtp->Host = $configuration->Host;
				$smtp->Port = $configuration->Port;
				$smtp->Username = $configuration->Username;
				$smtp->Password = $configuration->Password;

				$smtp->SetFrom(mail, siteName);
				$smtp->Subject = $mail->getAttr('subject');
				$smtp->Body = $mail->getAttr('body');
				$smtp->AddAddress($mail->getAttr('to'));
				if($smtp->Send()) {
					$go = 1;
				}

			} else {
				if(mail($mail->getAttr('to'), $mail->getAttr('subject'), $mail->getAttr('body'), $headers)) {
					$go = 1;
				}
			}

			if($go == 1) {
				$mail->setAttr('send', time());
				$mail->putExecute();
			}

		}
	}
	
}

define('footer_email', "</body></html>\n\n- - - \n" . siteName);

?>