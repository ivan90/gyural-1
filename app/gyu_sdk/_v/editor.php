<? if($_GET["done"] == 1): ?>
<strong>Applicazione Creata.</strong>
<? endif; ?>
<form method="post">
	<p>L'sdk ti aiuterà nella creazione e l'installazione delle app!</p>
	<p>All'interno di gyu_sdk/_sample puoi modificare i tuoi template.</p>
	<h3>Name dell'Applicazione</h3>
	<input type="text" name="user" placeholder="Il tuo nome" />
	<input type="text" name="email" placeholder="La tua e-mail" />
	<input type="text" name="appName" placeholder="Name dell'app." />
	<div class="appNameHelper"></div>
	<div class="rest" style="display: none">
		<label><input type="checkbox" name="global[ctrl]" value="1" /> Ctrl</label><br />
		<label><input type="checkbox" name="global[lib]" value="1" /> Lib</label><br />
		<label><input type="checkbox" name="global[funcs]" value="1" /> Functions</label><br />
		<label><input type="checkbox" name="global[hooks]" value="1" /> Hooks</label><br />
		<label><input type="checkbox" name="global[v]" value="1" /> Cartella: /_v/</label><br />
		<label><input type="checkbox" name="global[assets]" value="1" /> Cartella: /_assets/ (+js / +css)</label><br />
		<label><input type="checkbox" checked disabled /> Version.gapp</label><br />
		<h4>Sub</h4>
		<div class="sub_s"></div>
		<a href="javascript:;" class="add-sub">+ Aggiungi</a>
		<br />
		<input type="submit" value="Crea" class="button" />
	</div>
</form>

<script type="text/javascript">
	
	$(document).on('change', '[name=appName]', function() {
		$.get('/api/gyu_sdk.exists', { appName: $('[name=appName]').val() }, function(res) {
			if(undefined == res.error) {
				$('.rest').show();
			} else {
				$('.rest').hide();
			}
		}, 'json');
	});

	var index = 0;

	$(document).on('click', '.add-sub', function() {
		$('.sub_s').append('<input type="text" name="sub['+index+'][name]" placeholder="Name of sub '+$('[name=appName]').val()+'_" />');
		$('.sub_s').append('<label><input type="checkbox" name="sub['+index+'][ctrl]" value="1" /> Ctrl</label><br />');
		$('.sub_s').append('<label><input type="checkbox" name="sub['+index+'][lib]" value="1" /> Lib</label><br /><br />');
		index++;
	});

</script>