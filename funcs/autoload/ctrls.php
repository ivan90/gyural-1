<?php

/*

------------
Gyural 1.8
------------

Filename: /funcs/autoload/ctrls.php
 Version: 1.8
  Author: federicoq <f.quagliotto@mandarinoadv.com>
	Date: 24/01/2014

-----------
Controllers
-----------

*/ /*

-----
gDocs
-----

Func: LoadApp
Args: [$lib:string, *$istance:bool:false, *$args:array:null]
Desc: Load Application Controllers. Istance and Args are optional.. use the instance to call a single controller.

*/

function LoadApp($lib, $istance = false, $args = null) {
	
	if(strstr($lib, '/')) {
		$origLib = $lib;
		list($lib) = explode('/', $origLib);
		$secondLib = $lib . '_' . str_replace('/', '_', str_replace($lib . '/', '', $origLib));
	} else
		$secondLib = $lib;

	$name = $secondLib . 'Ctrl';
	$file = application . $lib . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $secondLib . '.ctrl.php';

	if(is_file($file)) {
		
		include_once $file;
		
		if($istance) {
			$res = new $name();
		} else
			$res = $name;
		
		return $res;

	}
	
	return;
	
}

/*

-----
gDocs
-----

Func: IsController
Args: [$page:string]
Desc: Check if an $page is an app controllers.

*/

function IsController($page) {
	
	$parts = explode('/', $page);
	foreach($parts as $v)
		if(strlen($v) > 0)
			$pts[] = $v;
			
	$Controller = $pts[0];
	
	if(HaveControllers($Controller)) {
		
		$ControllerApp = LoadController($Controller, 1);
		
		while(count($pts) > 1) {
			
			$suitable = $ControllerApp->__haveController($pts);
			if($suitable)
				return $suitable;
			
			unset($pts[count($pts)-1]);
			
		}
		
		if($ControllerApp->index_tollerant == true) {
			$parts[] = 'index';
			$suitable = $ControllerApp->__haveController($pts);
		}
		
		if($suitable)
			return $suitable;
		else
			return false;
		
	}
	
	return false;
}

/*

-----
gDocs
-----

Func: LoadController
Args: [$Controller:string, *$create:bool:false]
Desc: Maybe a copy of LoadApp

*/

function LoadController($Controller, $create = false) {
	
	if(strstr($Controller, 'Ctrl'))
		$Controller = str_replace('Ctrl', '', $Controller);
	
	$whereToLook = HaveControllers($Controller);
	if($whereToLook) {
		include_once $whereToLook;
		$Controller .= 'Ctrl';
	
		if($create == 1)
			return new $Controller;
		else
			return true;
		
	}
	return false;
	
}

function HaveControllers($controllerName) {
	
	$portions = explode('_', $controllerName);
	$counter = count($portions);
	$controllerDir = $controllerName;

	while($counter > 0) {

		unset($portions[$counter]);
		$controllerDir = implode('_', $portions);

		if(strlen($controllerDir) == 0)
			return false;
		
		if($controllerDir[0] != '*') {
			$specific = application . $controllerDir . DIRECTORY_SEPARATOR . '_' . DIRECTORY_SEPARATOR . $controllerName.'.ctrl.php';
		} else {
			$controllerName = str_replace('*', '', $controllerName);
			$specific = libs . $controllerName . ".ctrl.php";
		}
		
		$global = libs . $controllerName . ".ctrl.php";

		if(is_file($specific))
			return $specific;
		else if(is_file($global))
			return $global;

		$counter--;

	}

	return false;
	
}

?>